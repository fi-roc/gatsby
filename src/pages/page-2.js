import React from "react"
import { Link } from "gatsby"

import Layout from "../components/layout"
import SEO from "../components/seo"

const SecondPage = () => (
  <Layout>
    <h1>Title</h1>
    <p>Description</p>

    <Link to="/">Go back to the homepage</Link>
  </Layout>
)

export default SecondPage
